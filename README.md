# sast-extras

Community SAST scanners in GitLab CI

## 

|Name/Link|Owner|License|Platforms|Note|
|--- |--- |--- |--- |--- |
|[.NET Security Guard](https://security-code-scan.github.io/)||Open Source or Free||.NET, C\#, VB.net|
|[APIsecurity.io Security Audit](https://apisecurity.io/tools/audit/)||Open Source or Free||online tool for OpenAPI / Swagger file static security analysis|
|[Coverity](https://www.synopsys.com/software-integrity/security-testing/static-analysis-sast.html)||Open Source or Free||Android, C\#, C, C++, Java, JavaScript, Node.js, Objective-C, PHP, Python, Ruby, Scala, Swift, VB.NET|
|[Dawnscanner](https://rubygems.org/gems/dawnscanner)||Open Source or Free||Dawnscanner is an open source security source code analyzer for Ruby, supporting major MVC frameworks like Ruby on Rails, Padrino, and Sinatra. It also works on non-web applications written in Ruby.|
|[Deep Dive](https://discotek.ca/deepdive.xhtml)||Open Source or Free||Byte code analysis tool for discovering vulnerabilities in Java deployments (EAR, WAR, JAR).|
|[Enlightn](https://www.laravel-enlightn.com/)|Enlightn Software|Open Source||Enlightn is a vulnerability scanner specifically designed for Laravel PHP applications that combines SAST, DAST, IAST and configuration analysis techniques to detect vulnerabilities.|
|[Fluid Attack's Scanner](https://docs.fluidattacks.com/machine/scanner)|Fluid Attacks|Open Source||SAST, DAST and SCA vulnerability detection tool with perfect OWASP Benchmark score.|
|[GolangCI-Lint](https://golangci-lint.run/)||Open Source or Free||A Go Linters aggregator - One of the Linters is [gosec (Go Security)](https://github.com/securego/gosec), which is off by default but can easily be enabled.|
|[Google CodeSearchDiggity](https://www.bishopfox.com/resources/tools/google-hacking-diggity/attack-tools/)||Open Source or Free||Uses Google Code Search to identify vulnerabilities in open source code projects hosted by Google Code, MS CodePlex, SourceForge, Github, and more. The tool comes with over 130 default searches that identify SQL injection, cross-site scripting (XSS), insecure remote and local file includes, hard-coded passwords, and much more. *Essentially, Google CodeSearchDiggity provides a source code security analysis of nearly every single open source code project in existence – simultaneously.*|
|[Graudit](https://github.com/wireghoul/graudit/)||Open Source or Free|Linux|Scans multiple languages for various security flaws. Basically security enhanced code Grep.|
|[Horusec](https://github.com/ZupIT/horusec)||Open Source or Free||Python(3.x), Ruby, Javascript, GoLang, .NetCore(3.x), Java, Kotlin, Terraform|
|[HuskyCI](https://huskyci.opensource.globo.com/)||Open Source or Free||HuskyCI is an open-source tool that orchestrates security tests inside CI pipelines of multiple projects and centralizes all results into a database for further analysis and metrics. HuskyCI can perform static security analysis in Python (Bandit and Safety), Ruby (Brakeman), JavaScript (Npm Audit and Yarn Audit), Golang (Gosec), and Java(SpotBugs plus Find Sec Bugs)|
|[Insider CLI](https://github.com/insidersec/insider)|InsiderSec|Open Source or Free||A open source Static Application Security Testing tool (SAST) written in GoLang for Java Maven and Android), Kotlin (Android), Swift (iOS), .NET Full Framework, C#, and Javascript (Node.js).|
|[LGTM](https://lgtm.com/help/lgtm/about-lgtm)||Open Source or Free||A free for open source static analysis service that automatically monitors commits to publicly accessible code in Bitbucket Cloud, GitHub, or GitLab. Supports C/C++, C\#, Go, Java, JavaScript/TypeScript, Python.|
|[OWASP ASST (Automated Software Security Toolkit)](https://github.com/OWASP/ASST)|Tarik Seyceri & OWASP|Open Source or Free|Ubuntu, MacOSX and Windows|An Open Source, Source Code Scanning Tool, developed with JavaScript (Node.js framework), Scans for PHP & MySQL Security Vulnerabilities According to OWASP Top 10 and Some other OWASP's famous vulnerabilities, and it teaches developers of how to secure their codes after scan.|
|[OWASP Code Crawler](https://wiki.owasp.org/index.php/Category:OWASP_Code_Crawler)|OWASP|Open Source||.NET, Java|
|[OWASP LAPSE Project](https://wiki.owasp.org/index.php/OWASP_LAPSE_Project)|OWASP|Open Source||Java|
|[OWASP Orizon Project](https://wiki.owasp.org/index.php/Category:OWASP_Orizon_Project)|OWASP|Open Source||Java|
|[OWASP WAP (Web Application Protection)](https://wiki.owasp.org/index.php/OWASP_WAP-Web_Application_Protection)|OWASP|Open Source||PHP|
|[ParaSoft](https://www.parasoft.com/)||Open Source or Free||C, C++, Java, .NET|
|[Polyspace Static Analysis](https://www.mathworks.com/products/polyspace.html)||Open Source or Free||C, C++, Ada|
|[Progpilot](https://github.com/designsecurity/progpilot)||Open Source or Free||Progpilot is a static analyzer tool for PHP that detects security vulnerabilities such as XSS and SQL Injection.|
|[Psalm](https://psalm.dev/docs/security_analysis/)|Vimeo, Inc.|Open Source||Static code analysis for PHP projects, written in PHP.|
|[Pyre](https://pyre-check.org/)||Open Source or Free||A performant type-checker for Python 3, that also has [limited security/data flow analysis](https://pyre-check.org/docs/pysa-basics.html) capabilities.|
|[ShiftLeft Scan](https://github.com/ShiftLeftSecurity/sast-scan)||Open Source or Free||A free open-source DevSecOps platform for detecting security issues in source ode and dependencies. It supports a broad range of languages and CI/CD pipelines by bundling various open source scanners into the pipeline.|
|[SonarCloud](https://sonarcloud.io/about)||Open Source or Free||ABAP, C, C++, Objective-C, COBOL, C\#, CSS, Flex, Go, HTML, Java, Javascript, Kotlin, PHP, PL/I, PL/SQL, Python, RPG, Ruby, Swift, T-SQL, TypeScript, VB6, VB, XML|
|[SonarQube](https://www.sonarqube.org/downloads/)||Open Source or Free||Scans source code for 15 languages for Bugs, Vulnerabilities, and Code Smells. SonarQube IDE plugins for Eclipse, Visual Studio, and IntelliJ provided by [SonarLint](https://www.sonarlint.org/).|
|[VisualCodeGrepper (VCG)](https://sourceforge.net/projects/visualcodegrepp/)||Open Source or Free||Scans C/C++, C\#, VB, PHP, Java, PL/SQL, and COBOL for security issues and for comments which may indicate defective code. The config files can be used to carry out additional checks for banned functions or functions which commonly cause security issues.|

